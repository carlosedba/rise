<?php $__env->startSection('title'); ?>
	Notícias - Hope Football Club
<?php $__env->stopSection(); ?>

<?php $__env->startSection('description'); ?>
	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('vendor-css'); ?>
	##parent-placeholder-97688f63ed1a87ba587e78933c42edf42ecae775##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('application-css'); ?>
	##parent-placeholder-b51d72c3ca446ab0f6f653f45ff8b7eb92a61211##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('fonts'); ?>
	##parent-placeholder-04d3b602cdc8d51e1a3bb4d03f7dab96a9ec37e5##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	##parent-placeholder-040f06fd774092478d450774f5ba30c5da78acc8##

	<!-- Hero -->
	<section class="page-header page-header-alpha" style="background-image: url('http://localhost/rise/server/public_html//themes/hopefc/resources/img/kids/indice4.jpg');">
		<div class="page-header-overlay"></div>
		<div class="page-header-wrapper">
			<p class="header-title">Notícias</p>
            <div class="breadcrumbs">
                <a class="breadcrumbs-item" href="javascript:void(0)">Início</a>
                <a class="breadcrumbs-item active" href="javascript:void(0)">Notícias</a>
            </div>
		</div>
	</section>

	<section class="section section-noticias">
		<div class="section-wrapper">
			<div class="section-row no-pad">
				<div class="grid grid--center">
					<?php 
					$configs = Model::factory('Config')->findArray();
					var_dump($configs);
					 ?>
					<div class="newscard newscard-alpha" style="background-image: url('resources/img/fb/27336955_148303799213332_96015138361565882_n.jpg')">
						<div class="newscard-alpha-overlay"></div>
						<span class="newscard-alpha-tag hidden">Institucional</span>
						<a class="newscard-alpha-title" href="#">Dr. Diego Portugal entra para o Hope Football Club</a>
						<p class="newscard-alpha-description">Um dos médicos mais respeitados e conceituados pelos desportistas do Brasil</p>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer-sections'); ?>
	##parent-placeholder-2b652bef4ae9be2d3a3fe2f169e6a106e1d3faee##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>