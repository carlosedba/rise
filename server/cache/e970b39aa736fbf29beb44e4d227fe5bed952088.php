<head>
	<meta charset="utf-8">
	<title><?php echo $__env->yieldContent('title'); ?></title>
	<meta name="description" content="<?php echo $__env->yieldContent('description'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="ClearType" content="true">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<?php $__env->startSection('vendor-css'); ?>
		<!-- Vendor CSS -->
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/hopefc/vendor/normalize/normalize.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/hopefc/vendor/hamburgers/hamburgers.min.css">
		<?php echo $__env->yieldSection(); ?>

	<?php $__env->startSection('application-css'); ?>
		<!-- Application CSS -->
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/hopefc/resources/css/anim.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/hopefc/resources/css/colors.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/hopefc/resources/css/buttons.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/hopefc/resources/css/inputs.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/hopefc/resources/css/main.css">
		<?php echo $__env->yieldSection(); ?>

	<?php $__env->startSection('fonts'); ?>
		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Cairo:300,700" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/hopefc/resources/fonts/nexa/nexa.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/hopefc/resources/fonts/hudsonny/hudsonny.css">
		<?php echo $__env->yieldSection(); ?>

	<?php $__env->startSection('js'); ?>
		<script src="http://localhost/rise/server/public_html//themes/hopefc/vendor/modernizr/modernizr-custom.js"></script>
		<?php echo $__env->yieldSection(); ?>
</head>