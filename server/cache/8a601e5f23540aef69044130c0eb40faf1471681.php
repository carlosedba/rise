<head>
	<meta charset="utf-8">
	<title><?php echo $__env->yieldContent('title'); ?></title>
	<meta name="description" content="<?php echo $__env->yieldContent('description'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="ClearType" content="true">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<?php $__env->startSection('vendor-css'); ?>
		<!-- Vendor CSS -->
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/uplay/vendor/normalize/normalize.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/uplay/vendor/hamburgers/hamburgers.min.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/uplay/vendor/tiny-slider/tiny-slider.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/uplay/vendor/flickity/flickity.min.css">
		<?php echo $__env->yieldSection(); ?>

	<?php $__env->startSection('application-css'); ?>
		<!-- Application CSS -->
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/uplay/resources/css/buttons.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/uplay/resources/css/inputs.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/uplay/resources/css/main.css">
		<?php echo $__env->yieldSection(); ?>

	<?php $__env->startSection('fonts'); ?>
		<!-- Fonts -->
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/uplay/resources/fonts/poppins/stylesheet.css">
		<link rel="stylesheet" type="text/css" href="http://localhost/rise/server/public_html//themes/uplay/resources/fonts/futura/stylesheet.css">
		<?php echo $__env->yieldSection(); ?>

	<?php $__env->startSection('js'); ?>
		<script src="http://localhost/rise/server/public_html//themes/uplay/vendor/modernizr/modernizr-custom.js"></script>
		<?php echo $__env->yieldSection(); ?>
</head>