<?php $__env->startSection('title'); ?>
	Academia Uplay Fitness
<?php $__env->stopSection(); ?>

<?php $__env->startSection('description'); ?>
	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('vendor-css'); ?>
	##parent-placeholder-97688f63ed1a87ba587e78933c42edf42ecae775##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('application-css'); ?>
	##parent-placeholder-b51d72c3ca446ab0f6f653f45ff8b7eb92a61211##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('fonts'); ?>
	##parent-placeholder-04d3b602cdc8d51e1a3bb4d03f7dab96a9ec37e5##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	##parent-placeholder-040f06fd774092478d450774f5ba30c5da78acc8##

	<section class="page-header page-header-alpha" style="background-image: url('resources/img/guarapuava/JR017_0024.jpg');">
		<div class="page-header-overlay"></div>
		<div class="page-header-wrapper">
			<p class="header-title">Invista na Uplay</p>
            <div class="breadcrumbs">
                <a class="breadcrumbs-item" href="javascript:void(0)">Início</a>
                <a class="breadcrumbs-item active" href="javascript:void(0)">Invista na Uplay</a>
            </div>
		</div>
	</section>

	<section class="section">
		<div class="section-wrapper">
			<div class="text-content center">
				<p class="text-title">Mercado Fitness</p>
				<p>
					Em média, apenas 5% da população brasileira frequenta academias, ou pouco mais de 7 milhões de pessoas. Nas grandes capitais, esse percentual chega a 15% da população. Com a mobilidade social proporcionada pelo avanço econômico dos últimos anos, 74% dos brasileiros encontram-se entre as classes A e C, o que representa 140 milhões de potenciais consumidores.
				</p><br>
				<p>
					Com nossa política de preços, tornamos esse mercado acessível para toda essa fatia da população, e dessa forma, construímos um mercado forte e resiliente mesmo diante de crises econômicas.
				</p><br>
				<p>
					Oferecemos academias de alto padrão e uma proposta de serviço diferenciado aos alunos, possuindo assim grande retenção.
				</p><br>
				<p>
					Através de nossa expertise na escolha do ponto, utilizando métricas avançadas (as mesmas utilizadas pelo Facebook, Google E Instagram), garantimos o sucesso de sua unidade.
				</p><br>
				<p>
					Nossos pilares baseiam-se em: Padronização, Baixo Risco de execução, Processos administrativos tecnológicos de gestão e suporte, Preço justo (baseado no melhor custo benefício regional), retenção de alunos, Espaço otimizado, Sonoridade exclusiva, Ambientação visual, Mídia digital, Abre todo dia, estacionamento, Alta Lucratividade e rápido retorno sobre o investimento.
				</p><br><br>
				<p class="text-title">Retorno sobre Investimento - ROI</p>
				<p>
					Um modelo de negócio baseado em Leasing reduz significativamente o investimento inicial de implantação. Os equipamentos podem ser substituídos a cada 5 anos, mantendo assim um padrão de atualização de tecnologia, o que garante uma grande vantagem competitiva em relação a concorrência e um ROI muito mais rápido, de apenas 1 ano.
				</p><br><br>
				<p class="text-title">Licença de uso de marca</p>
				<p>
					Nosso sistema é baseado em uma licensa de uso da marca, diferentemente do sistema de franquia, nesse formato o licenciado tem muito mais liberdade no sistema de gestão de seu próprio negócio.
				</p><br>
				<p>
					O valor pago pelo Leasing é deduzido diretamente do valor de licensa de uso de marca, assim, você paga apenas o valor excedente ao valor da parcela de Leasing.
				</p><br>
				<p>
					Dessa forma o valor cobrado é diretamente proporcional ao faturamento. Estamos verdadeiramente focados em ganhar com você, assim lutamos diariamente para aumentar o número de alunos de sua academia. O Fundo de propaganda previsto é de 1% do faturamento bruto.
				</p><br>
				<p>
					Alguns números que podem te ajudar entender melhor este negocio:
				</p><br>
				<p>
					Está interessado em investir nesse ramo que cresce mesmo diante das crises? Possuímos duas formas distintas para sua escolha:
				</p><br>
				<ul>
					<li><strong>Licensiado de marca:</strong> destinado ao empresário que deseja operar o próprio negocio, ele tem total poder de gestão e conta com auxilio e suporte nas áreas mais importantes, como Marketing, Vendas, Técnica e Gestão de pessoas.
					</li>
					<li><strong>Sócio investidor:</strong> destinado aos empresários que buscam apenas a rentabilidade de seu investimento. Nesse modelo a gestão é inteiramente realizada pela Uplay Fitness. Os resultados são apresentados e distribuídos periodicamente. Modelo de S.A. com emissão de debêntures.
					</li>
				</ul><br>
				<p>
					Maiores informações, acesse <a href="http://franquias.uplayfit.com" target="_blank">Franquias Uplay</a>. E entre em contato imediatamente para que possamos agendar uma reunião com nossa equipe de investimento pelo e-mail <a href="mailto:investidor@uplayfit.com">investidor@uplayfit.com</a>.
				</p>
			</div>
		</div>
	</section>

	<section class="section section-three hidden" style="background-image: url('resources/img/FB017_0001XY2.jpg')">
		<div class="section-wrapper">
			<div class="section-row small-pad">
				<div class="row-titles">
					<p class="row-title">Nossas Unidades</p>
				</div>
				<div class="section-cards">
					<div class="section-card section-card-one">
						<div class="card-picture" style="background-image: url('resources/img/IMG_5514X.jpg')"></div>
						<p class="card-title">Santa Felicidade</p>
						<div class="card-info">
							<label>Endereço:</label>
							<p>Av. Ver. Toaldo Túlio, 4595 - Orleans, Curitiba-PR</p>
						</div>
						<a class="btn" href="#">Ver mais</a>
					</div>
					<div class="section-card section-card-one">
						<div class="card-picture" style="background-image: url('resources/img/IMG_5514X.jpg')"></div>
						<p class="card-title">Santa Felicidade</p>
						<div class="card-info">
							<label>Endereço:</label>
							<p>Av. Ver. Toaldo Túlio, 4595 - Orleans, Curitiba-PR</p>
						</div>
						<a class="btn" href="#">Ver mais</a>
					</div>
					<div class="section-card section-card-one">
						<div class="card-picture" style="background-image: url('resources/img/IMG_5514X.jpg')"></div>
						<p class="card-title">Santa Felicidade</p>
						<div class="card-info">
							<label>Endereço:</label>
							<p>Av. Ver. Toaldo Túlio, 4595 - Orleans, Curitiba-PR</p>
						</div>
						<a class="btn" href="#">Ver mais</a>
					</div>
					<div class="section-card section-card-one">
						<div class="card-picture" style="background-image: url('resources/img/IMG_5514X.jpg')"></div>
						<p class="card-title">Santa Felicidade</p>
						<div class="card-info">
							<label>Endereço:</label>
							<p>Av. Ver. Toaldo Túlio, 4595 - Orleans, Curitiba-PR</p>
						</div>
						<a class="btn" href="#">Ver mais</a>
					</div>
					<div class="section-card section-card-one">
						<div class="card-picture" style="background-image: url('resources/img/IMG_5514X.jpg')"></div>
						<p class="card-title">Santa Felicidade</p>
						<div class="card-info">
							<label>Endereço:</label>
							<p>Av. Ver. Toaldo Túlio, 4595 - Orleans, Curitiba-PR</p>
						</div>
						<a class="btn" href="#">Ver mais</a>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer-sections'); ?>
	##parent-placeholder-2b652bef4ae9be2d3a3fe2f169e6a106e1d3faee##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>