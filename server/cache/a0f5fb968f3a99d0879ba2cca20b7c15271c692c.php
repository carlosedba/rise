<?php $__env->startSection('scripts'); ?>
	<!-- Scripts -->
	<script type="text/javascript" src="http://localhost/rise/server/public_html//themes/uplay/vendor/tiny-slider/min/tiny-slider.helper.ie8.js"></script>
	<script type="text/javascript" src="http://localhost/rise/server/public_html//themes/uplay/vendor/tiny-slider/min/tiny-slider.js"></script>
	<script type="text/javascript" src="http://localhost/rise/server/public_html//themes/uplay/vendor/flickity/flickity.pkgd.min.js"></script>
	<script type="text/javascript">
		function setHeroSlider() {
			const slider = tns({
				//autoHeight: true,
				container: '.hero-slides',
				items: 1,
				slideBy: 'page',
				autoplay: true,
				autoplayTimeout: 5000,
				autoplayButtonOutput: false,
				speed: 560,
				controlsText: ['<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.7 62.2"><path fill="#fff" d="M19 2.8l-.8-1.4L1.7 31.1l16.5 29.7.8-1.4L3.2 31.1z"/></svg>', '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.7 62.2"><path fill="#fff" d="M17.4 31.1L1.7 59.4l.7 1.4L19 31.1 2.4 1.4l-.7 1.4z"/></svg>']
			})
		}

		function setUnidadesMouseDrag() {
			new Flickity('#unidades .section-cards', {
				freeScroll: true,
				wrapAround: true,
				pageDots: false,
				percentPosition: false,
				prevNextButtons: false,
				friction: 0.22,
				selectedAttraction: 0.015,
				autoPlay: 2600,
				cellAlign: 'left',
				arrowShape: 'M65.1 2.4L63.7 0 35.9 50l27.8 50 1.3-2.4L38.5 50 65.1 2.4z',
			})
		}

		function setupHamburgerMenu() {
			const btn = document.querySelector('.hamburger')
			const menu = document.querySelector('.navbar-menu')
			const links = document.querySelectorAll('.navbar-menu a')

			if (btn !== null && menu != null) {
				btn.addEventListener('click', function (e) {
					btn.classList.toggle('is-active')
					menu.classList.toggle('is-open')
				})

				;[].forEach.call(links, function (el, i) {
					el.addEventListener('click', function (e) {
						btn.classList.remove('is-active')
						menu.classList.remove('is-open')
					})
				})
			} 
		}

		setHeroSlider()
		setupHamburgerMenu()
		setUnidadesMouseDrag()
	</script>
	<?php echo $__env->yieldSection(); ?>