<?php
namespace Rise;

use \PDO;
use \ORM;
use Rise\Model;

class Container extends \Slim\Container
{
	private $config;
	private $renderer;

	public function __construct(array $values = [])
	{
		parent::__construct(array('settings' => $values['netcore']));

		$this->setConfig($values);
		$this->initializeDatabase();
		$this->setStore();
		$this->setRenderer();
		$this->setAliases();
	}

	private function setConfig(array $values = [])
	{
		$this->config = $values;
	}

	public function initializeDatabase()
	{
		ORM::configure("{$this->config['db']['driver']}:host={$this->config['db']['host']};dbname={$this->config['db']['database']}");
		ORM::configure('username', $this->config['db']['username']);
		ORM::configure('password', $this->config['db']['password']);
		ORM::configure('return_result_sets', true);
		ORM::configure('driver_options', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
		Model::$auto_prefix_models = 'Rise\\Models\\';
		Model::$short_table_names = true;
	}

	public function setStore()
	{
		$this->offsetSet('store', function () {
			return new Store();
		});
	}

	public function setRenderer()
	{
		$store = $this->get('store');

		$this->renderer = new Renderer($this->config['renderer'], $store);
	}

	public function setAliases()
	{
		$this->offsetSet('internal', function () {
			return $this->renderer->internal();
		});

		$this->offsetSet('theme', function () {
			return $this->renderer->theme();
		});
	}
}
?>
