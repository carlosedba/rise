<?php
namespace Rise\Api;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Model;
use Rise\Utils\IdGenerator;

class Attributes
{
	public static function findAll(Request $request, Response $response, $args)
	{
		$attributes = Model::factory('Attribute')->findArray();
		$json = json_encode($attributes);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public static function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$attributes = Model::factory('Attribute')->where('id', $id)->findOne()->asArray();
		$json = json_encode($attributes);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}
	
	public static function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$attribute = Model::factory('Attribute')->create(array(
			'name' 			=> $data['name'],
		));

		if ($attribute->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public static function update(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$attribute = Model::factory('Attribute')
				->where('id', $id)
				->findOne()
				->fillAttributes($data);

		if ($attribute->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public static function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$attribute = Model::factory('Attribute')->where('id', $id)->findOne();

		if ($attribute->delete()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
