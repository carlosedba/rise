<?php
namespace Rise\Api;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Model;
use Rise\Utils\IdGenerator;

class Pages
{
	public static function findAll(Request $request, Response $response, $args)
	{
		$pages = Model::factory('Page')->findArray();
		$json = json_encode($pages);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public static function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$pages = Model::factory('Page')->where('id', $id)->findOne()->asArray();
		$json = json_encode($pages);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}
	
	public static function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$page = Model::factory('Page')->create(array(
			'id' 			=> IdGenerator::uniqueId(8),
			'name' 			=> $data['name'],
			'slug' 			=> $data['slug'],
			'collection_id' => $data['collection_id'],
		));

		if ($page->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public static function update(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$page = Model::factory('Page')
				->where('id', $id)
				->findOne()
				->fillAttributes($data);

		if ($page->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public static function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$page = Model::factory('Page')->where('id', $id)->findOne();

		if ($page->delete()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
