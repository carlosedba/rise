<?php
namespace Rise\Api;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Rise\Model;
use Rise\Utils\IdGenerator;

class Config
{
	public static function findAll(Request $request, Response $response, $args)
	{
		$configs = Model::factory('Config')->findArray();
		$json = json_encode($configs);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public static function findOneById(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$configs = Model::factory('Config')->where('id', $id)->findOne()->asArray();
		$json = json_encode($configs);
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}
	
	public static function create(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$config = Model::factory('Config')->create(array(
			'id' 			=> IdGenerator::uniqueId(8),
			'name' 			=> $data['name'],
			'value' 		=> $data['value'],
			'autoload' 		=> $data['autoload'],
		));

		if ($config->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public static function update(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$config = Model::factory('Config')
				->where('id', $id)
				->findOne()
				->fillAttributes($data);

		if ($config->save()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public static function delete(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$id = $args['id'];

		$config = Model::factory('Config')->where('id', $id)->findOne();

		if ($config->delete()) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}
?>
