<?php

namespace Rise\Utils;

class Str
{
	static public function dashesToCamelCase($string, $capitalizeFirstCharacter = false) 
	{
		$str = str_replace('-', '', ucwords($string, '-'));

		if (!$capitalizeFirstCharacter) {
			$str = lcfirst($str);
		}

		return $str;
	}

	static public function underscoreToCamelCase($string, $capitalizeFirstCharacter = false) 
	{
		$str = str_replace('_', '', ucwords($string, '_'));

		if (!$capitalizeFirstCharacter) {
			$str = lcfirst($str);
		}

		return $str;
	}
}
?>