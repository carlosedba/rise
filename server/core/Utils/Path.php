<?php
namespace Rise\Utils;

/**
* Path
*/
class Path
{
	// http://www.bin-co.com/php/scripts/filesystem/join_path/
	static public function join()
	{
		$path = '';
		$arguments = func_get_args();
		$args = array();
		foreach($arguments as $a) if($a !== '') $args[] = $a;//Removes the empty elements

		$arg_count = count($args);
		for($i=0; $i<$arg_count; $i++) {
			$folder = $args[$i];

			if($i != 0 and $folder[0] == DIRECTORY_SEPARATOR) $folder = substr($folder,1); //Remove the first char if it is a '/' - and its not in the first argument
			if($i != $arg_count-1 and substr($folder,-1) == DIRECTORY_SEPARATOR) $folder = substr($folder,0,-1); //Remove the last char - if its not in the last argument

			$path .= $folder;
			if($i != $arg_count-1) $path .= DIRECTORY_SEPARATOR; //Add the '/' if its not the last element.
		}
		return $path;
	}
}
?>
