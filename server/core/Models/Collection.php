<?php
namespace Rise\Models;

use Rise\Model;

class Collection extends Model
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'rise_collections';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'description', 'picture',
    ];
}
?>