<?php
namespace Rise;

class Theme
{
    public static function getManifest(Store $store)
    {
        $current = $store->get('config.theme');
        $manifest = json_decode(file_get_contents(THEMES_PATH . $current . '/manifest.json'));
        
        return $manifest;
    }

    public static function getCurrent(Store $store)
    {
        return $store->get('config.theme');
    }
}
?>
