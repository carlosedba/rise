var heeSlider = function (element, options) {
	this.dom = {
		slider: element,
		slides: element.querySelector('.slides'),
		slide: element.querySelectorAll('.slides > .slide'),
		controlPrev: element.querySelector('.controls > .prev'),
		controlNext: element.querySelector('.controls > .next'),
		counter: element.querySelector('.counter'),
		slideball: element.querySelectorAll('.counter > .slideball')
	}

	this.indexes = {
		total: null,
		prev: null,
		current: null,
		next: null
	}

	this.states = ['hidden-prev', 'prev', 'current', 'next', 'hidden-next']

	this.loop = null

	this.config = {
		delay: 4500
	}

	this.setConfig(options).setIndexes().setSlides().setControls().createSlideBalls().setSlideballs().setLoop()
}

heeSlider.prototype.setConfig = function (options) {
	for (var i in options) {
		this.config[i] = options[i];
	}

	return this
}

heeSlider.prototype.setIndexes = function () {
	this.indexes.total = this.dom.slide.length

	if (this.indexes.prev === null && this.indexes.current === null && this.indexes.next === null) {
		
		if (this.indexes.total > 2) {
			this.indexes.current = Math.floor(this.indexes.total / 2)
		} else {
			this.indexes.current = 0
		}
		
		if (this.indexes.current > 0) {
			this.indexes.prev = this.indexes.current - 1
		} else {
			this.indexes.prev = null
		}

		if (this.indexes.current < this.indexes.total - 1) {
			this.indexes.next = this.indexes.current + 1
		} else {
			this.indexes.next = null
		}
	}

	return this
}

heeSlider.prototype.updateIndexes = function (index, callback) {
	this.indexes.current = index

	if (this.indexes.current > 0) {
		this.indexes.prev = this.indexes.current - 1
	} else {
		this.indexes.prev = null
	}

	if (this.indexes.current < this.indexes.total - 1) {
		this.indexes.next = this.indexes.current + 1
	} else {
		this.indexes.next = null
	}

	if (callback) callback()

	return this;
}

heeSlider.prototype.clearState = function (slide) {
	this.states.forEach(function (state, ind, arr) {
		this.dom.slide[slide].classList.remove(state)
	}.bind(this))

	return this
}

heeSlider.prototype.setSlides = function () {
	;[].forEach.call(this.dom.slide, function (slide, ind, arr) {
		if (ind < this.indexes.prev) slide.classList.add('hidden-prev')
		if (ind === this.indexes.prev) slide.classList.add('prev')
		if (ind === this.indexes.current) slide.classList.add('current')
		if (ind === this.indexes.next) slide.classList.add('next')
		if (ind > this.indexes.next) slide.classList.add('hidden-next')
	}, this)

	return this
}

heeSlider.prototype.updateSlide = function (next) {
	if (this.indexes.current > 0) {
		this.clearState(this.indexes.prev)
		this.dom.slide[this.indexes.prev].classList.add('prev')
	}

	this.clearState(this.indexes.current)
	this.dom.slide[this.indexes.current].classList.add('current')

	if (next) {
		if (this.indexes.current < this.indexes.total - 1) {
			setTimeout(function () {
				this.clearState(this.indexes.next)
				this.dom.slide[this.indexes.next].classList.add('next')
			}.bind(this), 800)
		}
	} else {
		if (this.indexes.current < this.indexes.total - 1) {
			this.clearState(this.indexes.next)
			this.dom.slide[this.indexes.next].classList.add('next')
		}
	}

	;[].forEach.call(this.dom.slide, function (slide, ind, arr) {
		if (
			ind !== this.indexes.prev &&
			ind !== this.indexes.current &&
			ind !== this.indexes.next
		) {
			this.clearState(ind)
			if (ind < this.indexes.prev) slide.classList.add('hidden-prev')
			if (ind > this.indexes.next) slide.classList.add('hidden-next')
		}
	}, this)

	return this
}

heeSlider.prototype.setControls = function () {
	this.dom.controlPrev.addEventListener('click', this.prev.bind(this))
	this.dom.controlNext.addEventListener('click', this.next.bind(this))

	return this
}

heeSlider.prototype.prev = function () {
	if (this.indexes.prev !== null) {
		this.updateIndexes(this.indexes.prev)
		this.updateSlide()
		this.updateSlideballs(this.indexes.current)
		this.resetLoop()
	} else {
		this.updateIndexes(this.indexes.total - 1)
		this.updateSlide(1)
		this.updateSlideballs(this.indexes.current)
		this.resetLoop()
	}

	return this
}

heeSlider.prototype.next = function () {
	if (this.indexes.next !== null) {
		this.updateIndexes(this.indexes.next)
		this.updateSlide(1)
		this.updateSlideballs(this.indexes.current)
		this.resetLoop()
	} else {
		this.updateIndexes(0)
		this.updateSlide(1)
		this.updateSlideballs(this.indexes.current)
		this.resetLoop()
	}

	return this
}

heeSlider.prototype.goTo = function (e) {
	var slide = e.target.dataset.slide - 1
	this.updateIndexes(slide)
	this.updateSlide()
	this.updateSlideballs(slide)
	this.resetLoop()

	return this
}

heeSlider.prototype.setLoop = function () {
	this.loop = setInterval(function () {
		if (this.indexes.next !== null) {
			this.updateIndexes(this.indexes.next)
			this.updateSlide()
			this.updateSlideballs(this.indexes.current)
		} else {
			this.updateIndexes(0)
			this.updateSlide()
			this.updateSlideballs(0)
		}
	}.bind(this), this.config.delay)

	return this
}

heeSlider.prototype.resetLoop = function () {
	clearInterval(this.loop)
	this.setLoop()

	return this
}

heeSlider.prototype.createSlideBalls = function () {
	console.log(this)
	;[].forEach.call(this.dom.slide, function (slide, ind, arr) {
		var slideball = document.createElement('a')
		slideball.href = 'javascript:void(0)'
		slideball.classList.add('slideball')
		slideball.dataset.slide = ind + 1
		slideball.addEventListener('click', this.goTo.bind(this))
		this.dom.counter.appendChild(slideball)
	}, this)

	return this
}

heeSlider.prototype.setSlideballs = function () {
	this.dom.slideball = this.dom.slider.querySelectorAll('.counter > .slideball')
	this.updateSlideballs(this.indexes.current)

	return this
}

heeSlider.prototype.updateSlideballs = function (slide) {
	;[].forEach.call(this.dom.slideball, function (slideball, ind, arr) {
		slideball.classList.remove('active')
	}, this)

	this.dom.slideball[slide].classList.add('active')

	return this
}

