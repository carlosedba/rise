const XEvent = window.XEvent = function () {
	console.log('log > Event - initialized!')

	this.Handlers = Object.create(Object.prototype)
}

XEvent.prototype.getHandler = function (name) {
	return this.Handlers[name]
}

XEvent.prototype.addHandler = function (name, func, context) {
	if (context === null || context === undefined) context = window
	return Object.defineProperty(this.Handlers, name, { value: func.bind(context) })
}

XEvent.prototype.removeHandler = function (name, func) {
	return delete this.Handlers[name]
}

XEvent.prototype.addTo = function (node, eventType, handlerName, propagation) {
	if (propagation === null || propagation === undefined) propagation = false
	if (node !== undefined, eventType !== undefined, handlerName !== undefined) {
		if (node.length === undefined) {
			node.addEventListener(eventType, this.getHandler(handlerName))
		} else {
			let nodes = [].slice.call(node)
			nodes.map(function (el, i) { el.addEventListener(eventType, this.getHandler(handlerName)), propagation }.bind(this))
		}
	}
}

XEvent.prototype.removeFrom = function (node, eventType, handlerName, propagation) {
	if (propagation === null || propagation === undefined) propagation = false
	if (node !== undefined, eventType !== undefined, handlerName !== undefined) {
		if (node.length === undefined) {
			node.removeEventListener(eventType, this.getHandler(handlerName))
		} else {
			let nodes = [].slice.call(node)
			nodes.map(function (el, i) { el.removeEventListener(eventType, this.getHandler(handlerName)), propagation }.bind(this))
		}
	}
}

