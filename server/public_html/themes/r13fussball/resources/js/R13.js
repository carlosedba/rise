// *************************************************** //
// * R13 ********************************************* //
// *************************************************** //

const R13 = (function () {
	'use strict'
		
	let defaults = {}
	
	let api = {
		init
	}
	

	function isInt(num) {
		if (num % 1 === 0) {
			return true
		} else if (num % 1 !== 0) {
			return false
		}
	}

	function extend(a, b) {
		for (var i in b) {
			a[i] = b[i]
		}
	}

	function getQueryParam(param) {
		let query = window.location.search.substring(1)
		let vars = query.split('&')

		for (let i = 0; i < vars.length; i++) {
			let pair = vars[i].split('=')

			if (decodeURIComponent(pair[0]) == param) {
				return decodeURIComponent(pair[1])
			}
		}
	}

	function setupHamburgerMenu() {
		let btn = document.querySelector('.hamburger')
		let menu = document.querySelector('.navbar-menu')

		if (btn !== null && menu != null) {
			btn.addEventListener('click', (e) => {
				btn.classList.toggle('is-active')
				menu.classList.toggle('navbar-menu-active')
			})
		} 
	}

	function init(options) {
		extend(defaults, options)
		setupHamburgerMenu()
	}

	return api
})()