@extends('layouts.default')

@section('title')
	@store(config.siteName)
@endsection

@section('description')
	@store(config.siteDescription)
@endsection

@section('vendor-css')
	@parent
@endsection

@section('application-css')
	@parent
@endsection

@section('fonts')
	@parent
@endsection

@section('content')
	@parent

	<!-- Hero -->
	<section class="hero">
		<div class="hero-slides">
			<div class="hero-slide" style="background-image: url('@asset('resources/img/1.png')')"></div>
			<div class="hero-slide" style="background-image: url('@asset('resources/img/2.png')')"></div>
			<div class="hero-slide" style="background-image: url('@asset('resources/img/3.png')')"></div>
			<div class="hero-slide" style="background-image: url('@asset('resources/img/4.png')')"></div>
			<div class="hero-slide" style="background-image: url('@asset('resources/img/5.png')')"></div>
		</div>
		<div class="hero-titles">
			<p class="hero-pretitle">Mais que uma academia</p>
			<p class="hero-title">Um estilo de vida!</p>
		</div>
	</section>

	<section class="section section-two">
		<div class="section-wrapper">
			<div class="section-row">
				<div class="section-column">
					<div class="column-titles">
						<p class="column-title">A academia que contagia</p>
						<p class="column-subtitle"></p>
						<p class="column-text">A <strong>#UPLAYFITNESS</strong> nasceu com um propósito muito claro: oferecer a seus associados a <strong>melhor relação custo benefício</strong> da região em que atua. Isso mesmo, diferente das grandes redes <strong>#LOWCOST</strong>, que entopem suas unidades com alunos e deixam a desejar no atendimento, nós somos como as <strong>#SMARTCOST</strong>, oferecendo a escolha mais inteligente para você e sua família.</p>
					</div>
				</div>
				<div class="section-column">
					<div class="column-lateral-image" style="background-image: url('@asset('resources/img/FB017_0029X.jpg')')"></div>
				</div>
			</div>
		</div>
	</section>

	<section id="unidades" class="section section-three" style="background-image: url('@asset('resources/img/FB017_0001XY2.jpg')')">
		<div class="section-wrapper">
			<div class="section-row small-pad">
				<div class="row-titles">
					<p class="row-title">Nossas Unidades</p>
				</div>
				<div class="section-cards">
					<div class="section-card section-card-one">
						<div class="card-picture" style="background-image: url('@asset('resources/img/saobraz/2017-03-22-PHOTO-00006955.jpg')')"></div>
						<p class="card-title">São Braz</p>
						<div class="card-info">
							<label>Endereço:</label>
							<p>Av. Ver. Toaldo Túlio, 4595 - Orleans, Curitiba-PR</p>
						</div>
						<a class="btn" href="#">Ver mais</a>
					</div>
					<div class="section-card section-card-one">
						<div class="card-picture" style="background-image: url('@asset('resources/img/avbrasilia/2018-01-02-PHOTO-00000011.jpg')')"></div>
						<p class="card-title">Av. Brasília</p>
						<div class="card-info">
							<label>Endereço:</label>
							<p>Av. Brasília, 5664 - Capão Raso, Curitiba-PR</p>
						</div>
						<a class="btn" href="#">Ver mais</a>
					</div>
					<div class="section-card section-card-one">
						<div class="card-picture" style="background-image: url('@asset('resources/img/pinhais/2017-06-27-PHOTO-00011924.jpg')')"></div>
						<p class="card-title">Pinhais</p>
						<div class="card-info">
							<label>Endereço:</label>
							<p>Av. Iraí, 440 - Weissópolis, Pinhais-PR</p>
						</div>
						<a class="btn" href="#">Ver mais</a>
					</div>
					<div class="section-card section-card-one">
						<div class="card-picture" style="background-image: url('@asset('resources/img/joinville/IMG_5533.jpg')')"></div>
						<p class="card-title">Joinville</p>
						<div class="card-info">
							<label>Endereço:</label>
							<p>R. Dr. Plácido Olímpio de Oliveira, 594 - Bucarein, Joinville-SC</p>
						</div>
						<a class="btn" href="#">Ver mais</a>
					</div>
					<div class="section-card section-card-one">
						<div class="card-picture" style="background-image: url('@asset('resources/img/guarapuava/2017-06-28-PHOTO-00011955.jpg')')"></div>
						<p class="card-title">Guarapuava</p>
						<div class="card-info">
							<label>Endereço:</label>
							<p>R. Xavier da Silva, 1983 - Centro, Guarapuava-PR</p>
						</div>
						<a class="btn" href="#">Ver mais</a>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('footer-sections')
	@parent
	<section class="section">
		<iframe class="footer-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3603.933361216637!2d-49.33320868550282!3d-25.407041138207134!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce1a3f2cf4fa7%3A0x74f6dfca0240a77!2sR.+Val%C3%A9rio+Haisi%2C+262+-+Santa+Felicidade%2C+Curitiba+-+PR%2C+82020-680!5e0!3m2!1spt-BR!2sbr!4v1515955818722" frameborder="0" style="border:0" allowfullscreen></iframe>
	</section>
@endsection

@section('scripts')
	@parent
@endsection