<head>
	<meta charset="utf-8">
	<title>@yield('title')</title>
	<meta name="description" content="@yield('description')">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="ClearType" content="true">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	@section('vendor-css')
		<!-- Vendor CSS -->
		<link rel="stylesheet" type="text/css" href="@asset('vendor/normalize/normalize.css')">
		<link rel="stylesheet" type="text/css" href="@asset('vendor/hamburgers/hamburgers.min.css')">
		@show

	@section('application-css')
		<!-- Application CSS -->
		<link rel="stylesheet" type="text/css" href="@asset('resources/css/anim.css')">
		<link rel="stylesheet" type="text/css" href="@asset('resources/css/colors.css')">
		<link rel="stylesheet" type="text/css" href="@asset('resources/css/buttons.css')">
		<link rel="stylesheet" type="text/css" href="@asset('resources/css/inputs.css')">
		<link rel="stylesheet" type="text/css" href="@asset('resources/css/main.css')">
		@show

	@section('fonts')
		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Cairo:300,700" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="@asset('resources/fonts/nexa/nexa.css')">
		<link rel="stylesheet" type="text/css" href="@asset('resources/fonts/hudsonny/hudsonny.css')">
		@show

	@section('js')
		<script src="@asset('vendor/modernizr/modernizr-custom.js')"></script>
		@show
</head>