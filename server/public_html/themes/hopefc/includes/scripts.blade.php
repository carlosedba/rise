@section('scripts')
	<!-- Application CSS -->
	<script type="text/javascript">
		function setupHamburgerMenu() {
			const btn = document.querySelector('.hamburger')
			const menu = document.querySelector('.navbar-menu')
			const links = document.querySelectorAll('.navbar-menu a')

			if (btn !== null && menu != null) {
				btn.addEventListener('click', function (e) {
					btn.classList.toggle('is-active')
					menu.classList.toggle('is-open')
				})

				;[].forEach.call(links, function (el, i) {
					el.addEventListener('click', function (e) {
						btn.classList.remove('is-active')
						menu.classList.remove('is-open')
					})
				})
			} 
		}

		setupHamburgerMenu()
	</script>
	@show