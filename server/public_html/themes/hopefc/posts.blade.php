@extends('layouts.default')

@section('title')
	Notícias - @store(config.siteName)
@endsection

@section('description')
	@store(config.siteDescription)
@endsection

@section('vendor-css')
	@parent
@endsection

@section('application-css')
	@parent
@endsection

@section('fonts')
	@parent
@endsection

@section('content')
	@parent

	<!-- Hero -->
	<section class="page-header page-header-alpha" style="background-image: url('@asset('resources/img/kids/indice4.jpg')');">
		<div class="page-header-overlay"></div>
		<div class="page-header-wrapper">
			<p class="header-title">Notícias</p>
            <div class="breadcrumbs">
                <a class="breadcrumbs-item" href="javascript:void(0)">Início</a>
                <a class="breadcrumbs-item active" href="javascript:void(0)">Notícias</a>
            </div>
		</div>
	</section>

	<section class="section section-noticias">
		<div class="section-wrapper">
			<div class="section-row no-pad">
				<div class="grid grid--center">
					@php
					$configs = Model::factory('Config')->findArray();
					var_dump($configs);
					@endphp
					<div class="newscard newscard-alpha" style="background-image: url('resources/img/fb/27336955_148303799213332_96015138361565882_n.jpg')">
						<div class="newscard-alpha-overlay"></div>
						<span class="newscard-alpha-tag hidden">Institucional</span>
						<a class="newscard-alpha-title" href="#">Dr. Diego Portugal entra para o Hope Football Club</a>
						<p class="newscard-alpha-description">Um dos médicos mais respeitados e conceituados pelos desportistas do Brasil</p>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('footer-sections')
	@parent
@endsection

@section('scripts')
	@parent
@endsection