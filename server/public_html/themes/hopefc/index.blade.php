@extends('layouts.default')

@section('title')
	@store(config.siteName)
@endsection

@section('description')
	@store(config.siteDescription)
@endsection

@section('vendor-css')
	@parent
@endsection

@section('application-css')
	@parent
@endsection

@section('fonts')
	@parent
@endsection

@section('content')
	@parent

	<!-- Hero -->
	<section class="hero" style="background-image: url('@asset('resources/img/kids/índice4.jpg')');">
		<p class="hero-text with-shadow">Participe de uma experiência única</p>
	</section>

	<section class="section orange">
		<div class="section-wrapper">
			<div class="section-row">
				<div class="row-titles">
					<p class="row-subtitle">O Hope Football Club é o mais novo clube de formação de atletas do futebol brasileiro. Temos como principal objetivo a formação de atletas de alta performance técnica e tática, em conjunto com o desenvolvimento intelectual e social de cada indivíduo.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="section lefthanded missao-visao">
		<div class="section-wrapper">
			<div class="section-row small-pad">
				<div class="row-titles">
					<p class="row-title">Nossa responsabilidade</p>
					<p class="row-subtitle">Desenvolver a mentalidade vencedora de atletas para alta performance.</p>
				</div>
			</div>
		</div>
	</section>

	<div class="section lefthanded">
		<div class="section-wrapper">
			<div class="section-row no-pad">
				<div class="section-line"></div>
			</div>
		</div>
	</div>

	<section class="section lefthanded missao-visao">
		<div class="section-wrapper">
			<div class="section-row small-pad">
				<div class="row-titles">
					<p class="row-title">Nosso objetivo</p>
					<p class="row-subtitle">Ter o maior Centro de Formação do Brasil até 2025 e o maior Centro de Revelação de jogadores do mundo até 2035.</p>
				</div>
			</div>
		</div>
	</section>

	<div class="section lefthanded">
		<div class="section-wrapper">
			<div class="section-row no-pad">
				<div class="section-line"></div>
			</div>
		</div>
	</div>

	<section class="section lefthanded missao-visao">
		<div class="section-wrapper">
			<div class="section-row small-pad">
				<div class="row-titles">
					<p class="row-title">Nossos princípios</p>
					<p class="row-subtitle">Verdade, Autorresponsabilidade, Excelência, Integridade, Caminhada, Humanidade, Comunicação, Princípios Cristãos, Clareza e Paixão.</p>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('footer-sections')
	@parent
	<section class="section">
		<iframe class="footer-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3603.933361216637!2d-49.33320868550282!3d-25.407041138207134!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce1a3f2cf4fa7%3A0x74f6dfca0240a77!2sR.+Val%C3%A9rio+Haisi%2C+262+-+Santa+Felicidade%2C+Curitiba+-+PR%2C+82020-680!5e0!3m2!1spt-BR!2sbr!4v1515955818722" frameborder="0" style="border:0" allowfullscreen></iframe>
	</section>
@endsection

@section('scripts')
	@parent
@endsection