@extends('layouts.default')

@section('title')
	@store(config.siteName)
@endsection

@section('description')
	@store(config.siteDescription)
@endsection

@section('vendor-css')
	@parent
@endsection

@section('application-css')
	@parent
@endsection

@section('fonts')
	@parent
@endsection

@section('content')
	@parent
@endsection

@section('footer-sections')
	@parent
@endsection

@section('scripts')
	@parent
@endsection