const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const CompressionPlugin = require('compression-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin')
const base = require('./webpack.config.base')

module.exports = function (env) {
	return merge(base(), {
		mode: 'development',

		devtool: 'cheap-module-eval-source-map',

		entry: [
			'react-hot-loader/patch',
			'webpack-dev-server/client?http://server.com:4000',
			'webpack/hot/only-dev-server',
			'./src/index.js'
		],

		output: {
			filename: 'app.bundle.js',
			sourceMapFilename: 'app.map'
		},

		devServer: {
			hot: true,
			contentBase: path.join(__dirname, 'dist'),
			publicPath: '/',
			compress: true,
			port: 4000,
			host: 'server.com',
			historyApiFallback: true
		},

		optimization: {
			minimize: false,
		},

		plugins: [
			new webpack.HotModuleReplacementPlugin(),

			new webpack.NamedModulesPlugin(),

			new webpack.DefinePlugin({
				PRODUCTION: false
			}),

			new webpack.EnvironmentPlugin({
				NODE_ENV: 'development'
			}),
		]
	})
}

