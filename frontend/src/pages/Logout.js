import React, { Component } from 'react'
import { connect } from 'react-redux'
import { logout } from '@/actions/session'

@connect((state) => {
  return {
    pending: state.session.auth.pending,
    logged: state.session.auth.authenticated,
  }
}, (dispatch, ownProps) => {
  return {
    logout() {
      return dispatch(logout())
    },
  }
})
export default class Logout extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.logout()

    const { history, pending, logged } = this.props
    if (!logged) history.replace('/')
  }

  componentDidUpdate(prevProps, prevState) {
    const { history, pending, logged } = this.props
    if (!logged) history.replace('/')
  }

  render() {
    return (
      <div className="page">
        <div className="page-loading">
          <div className="lds-dual-ring"></div>
        </div>
      </div>
    )
  }
}

