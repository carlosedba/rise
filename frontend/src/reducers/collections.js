import {
	FETCH_COLLECTIONS, FETCH_COLLECTIONS_LOADING, FETCH_COLLECTIONS_SUCCESS, FETCH_COLLECTIONS_ERROR,
	FETCH_COLLECTION, FETCH_COLLECTION_LOADING, FETCH_COLLECTION_SUCCESS, FETCH_COLLECTION_ERROR,
	CREATE_COLLECTION, CREATE_COLLECTION_LOADING, CREATE_COLLECTION_SUCCESS, CREATE_COLLECTION_ERROR,
	UPDATE_COLLECTION, UPDATE_COLLECTION_LOADING, UPDATE_COLLECTION_SUCCESS, UPDATE_COLLECTION_ERROR,
	DELETE_COLLECTION, DELETE_COLLECTION_LOADING, DELETE_COLLECTION_SUCCESS, DELETE_COLLECTION_ERROR
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case FETCH_COLLECTIONS:
		return { ...state, list: { items: [], error: null, loading: null } }

	case FETCH_COLLECTIONS_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case FETCH_COLLECTIONS_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case FETCH_COLLECTIONS_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case FETCH_COLLECTION:
		return { ...state, active: { item: null, error: null, loading: null } }

	case FETCH_COLLECTION_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case FETCH_COLLECTION_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case FETCH_COLLECTION_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case CREATE_COLLECTION:
		return { ...state, new: { item: null, error: null, loading: null } }

	case CREATE_COLLECTION_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case CREATE_COLLECTION_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case CREATE_COLLECTION_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case UPDATE_COLLECTION:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case UPDATE_COLLECTION_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case UPDATE_COLLECTION_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case UPDATE_COLLECTION_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case DELETE_COLLECTION:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case DELETE_COLLECTION_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case DELETE_COLLECTION_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case DELETE_COLLECTION_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }

	default:
		return state;
  }
}
