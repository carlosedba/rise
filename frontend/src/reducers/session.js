import {
  SESSION_FETCH_USER, SESSION_FETCH_USER_LOADING, SESSION_FETCH_USER_SUCCESS, SESSION_FETCH_USER_ERROR,
  SESSION_RENEW_TOKEN, SESSION_RENEW_TOKEN_LOADING, SESSION_RENEW_TOKEN_SUCCESS, SESSION_RENEW_TOKEN_ERROR,
  SESSION_FETCH_TOKEN, SESSION_FETCH_TOKEN_LOADING, SESSION_FETCH_TOKEN_SUCCESS, SESSION_FETCH_TOKEN_ERROR,
  SESSION_LOGOUT,
} from '../constants/ActionTypes'

const INITIAL_STATE = {
  auth: { pending: false, authenticated: false, token: null, error: null },
  user: { pending: false, data: null, error: null }
}

export default function(state = INITIAL_STATE, action) {
  let error, token

  switch (action.type) {
  case SESSION_RENEW_TOKEN:
    return { ...state, auth: { pending: true, authenticated: false, token: null, error: null } }

  case SESSION_RENEW_TOKEN_LOADING:
    return { ...state, auth: { pending: true, authenticated: false, token: null, error: null } }

  case SESSION_RENEW_TOKEN_SUCCESS:
    token = (action.payload.data.token) ? action.payload.data.token : null
    return { ...state, auth: { pending: false, authenticated: (token) ? true : false, token: token, error: null } }

  case SESSION_RENEW_TOKEN_ERROR:
    error = action.payload.response.data.error
    return { ...state, auth: { pending: false, authenticated: false, token: null, error: error } }


  case SESSION_FETCH_TOKEN:
    return { ...state, auth: { pending: true, authenticated: false, token: null, error: null } }

  case SESSION_FETCH_TOKEN_LOADING:
    return { ...state, auth: { pending: true, authenticated: false, token: null, error: null } }

  case SESSION_FETCH_TOKEN_SUCCESS:
    token = (action.payload.data.token) ? action.payload.data.token : null
    return { ...state, auth: { pending: false, authenticated: (token) ? true : false, token: token, error: null } }

  case SESSION_FETCH_TOKEN_ERROR:
    error = action.payload.response.data.error
    return { ...state, auth: { pending: false, authenticated: false, token: null, error: error } }


  case SESSION_FETCH_USER:
    return { ...state, user: { pending: true, data: null, error: null } }

  case SESSION_FETCH_USER_LOADING:
    return { ...state, user: { pending: true, data: null, error: null } }

  case SESSION_FETCH_USER_SUCCESS:
    return { ...state, user: { pending: false, data: action.payload.data, error: null } }

  case SESSION_FETCH_USER_ERROR:
    error = action.payload.response.data.error
    return { ...state, user: { pending: false, data: null, error: error } }


  case SESSION_LOGOUT:
    return { ...state, 
      auth: { pending: false, authenticated: false, token: null, error: null },
      user: { pending: false, data: null, error: null }
    }

  default:
    return state
  }
}
