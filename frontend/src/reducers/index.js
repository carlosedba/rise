import { combineReducers } from 'redux'

import session from './session'
import users from './users'

const reducer = combineReducers({
  session,
  users,
})

export default reducer

