import {
	FETCH_FIELD_TYPES, FETCH_FIELD_TYPES_LOADING, FETCH_FIELD_TYPES_SUCCESS, FETCH_FIELD_TYPES_ERROR,
	FETCH_FIELD_TYPE, FETCH_FIELD_TYPE_LOADING, FETCH_FIELD_TYPE_SUCCESS, FETCH_FIELD_TYPE_ERROR,
	CREATE_FIELD_TYPE, CREATE_FIELD_TYPE_LOADING, CREATE_FIELD_TYPE_SUCCESS, CREATE_FIELD_TYPE_ERROR,
	UPDATE_FIELD_TYPE, UPDATE_FIELD_TYPE_LOADING, UPDATE_FIELD_TYPE_SUCCESS, UPDATE_FIELD_TYPE_ERROR,
	DELETE_FIELD_TYPE, DELETE_FIELD_TYPE_LOADING, DELETE_FIELD_TYPE_SUCCESS, DELETE_FIELD_TYPE_ERROR
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case FETCH_FIELD_TYPES:
		return { ...state, list: { items: [], error: null, loading: null } }

	case FETCH_FIELD_TYPES_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case FETCH_FIELD_TYPES_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case FETCH_FIELD_TYPES_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case FETCH_FIELD_TYPE:
		return { ...state, active: { item: null, error: null, loading: null } }

	case FETCH_FIELD_TYPE_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case FETCH_FIELD_TYPE_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case FETCH_FIELD_TYPE_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case CREATE_FIELD_TYPE:
		return { ...state, new: { item: null, error: null, loading: null } }

	case CREATE_FIELD_TYPE_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case CREATE_FIELD_TYPE_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case CREATE_FIELD_TYPE_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case UPDATE_FIELD_TYPE:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case UPDATE_FIELD_TYPE_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case UPDATE_FIELD_TYPE_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case UPDATE_FIELD_TYPE_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case DELETE_FIELD_TYPE:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case DELETE_FIELD_TYPE_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case DELETE_FIELD_TYPE_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case DELETE_FIELD_TYPE_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }

	default:
		return state;
  }
}
