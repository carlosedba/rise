import {
	FETCH_ALL_CONTENT, FETCH_ALL_CONTENT_LOADING, FETCH_ALL_CONTENT_SUCCESS, FETCH_ALL_CONTENT_ERROR,
	FETCH_CONTENT, FETCH_CONTENT_LOADING, FETCH_CONTENT_SUCCESS, FETCH_CONTENT_ERROR,
	CREATE_CONTENT, CREATE_CONTENT_LOADING, CREATE_CONTENT_SUCCESS, CREATE_CONTENT_ERROR,
	UPDATE_CONTENT, UPDATE_CONTENT_LOADING, UPDATE_CONTENT_SUCCESS, UPDATE_CONTENT_ERROR,
	DELETE_CONTENT, DELETE_CONTENT_LOADING, DELETE_CONTENT_SUCCESS, DELETE_CONTENT_ERROR
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case FETCH_ALL_CONTENT:
		return { ...state, list: { items: [], error: null, loading: null } }

	case FETCH_ALL_CONTENT_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case FETCH_ALL_CONTENT_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case FETCH_ALL_CONTENT_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case FETCH_CONTENT:
		return { ...state, active: { item: null, error: null, loading: null } }

	case FETCH_CONTENT_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case FETCH_CONTENT_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case FETCH_CONTENT_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case CREATE_CONTENT:
		return { ...state, new: { item: null, error: null, loading: null } }

	case CREATE_CONTENT_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case CREATE_CONTENT_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case CREATE_CONTENT_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case UPDATE_CONTENT:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case UPDATE_CONTENT_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case UPDATE_CONTENT_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case UPDATE_CONTENT_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case DELETE_CONTENT:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case DELETE_CONTENT_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case DELETE_CONTENT_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case DELETE_CONTENT_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }

	default:
		return state;
  }
}
