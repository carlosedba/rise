import {
	FETCH_CONFIGS, FETCH_CONFIGS_LOADING, FETCH_CONFIGS_SUCCESS, FETCH_CONFIGS_ERROR,
	FETCH_CONFIG, FETCH_CONFIG_LOADING, FETCH_CONFIG_SUCCESS, FETCH_CONFIG_ERROR,
	CREATE_CONFIG, CREATE_CONFIG_LOADING, CREATE_CONFIG_SUCCESS, CREATE_CONFIG_ERROR,
	UPDATE_CONFIG, UPDATE_CONFIG_LOADING, UPDATE_CONFIG_SUCCESS, UPDATE_CONFIG_ERROR,
	DELETE_CONFIG, DELETE_CONFIG_LOADING, DELETE_CONFIG_SUCCESS, DELETE_CONFIG_ERROR
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case FETCH_CONFIGS:
		return { ...state, list: { items: [], error: null, loading: null } }

	case FETCH_CONFIGS_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case FETCH_CONFIGS_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case FETCH_CONFIGS_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case FETCH_CONFIG:
		return { ...state, active: { item: null, error: null, loading: null } }

	case FETCH_CONFIG_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case FETCH_CONFIG_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case FETCH_CONFIG_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case CREATE_CONFIG:
		return { ...state, new: { item: null, error: null, loading: null } }

	case CREATE_CONFIG_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case CREATE_CONFIG_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case CREATE_CONFIG_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case UPDATE_CONFIG:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case UPDATE_CONFIG_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case UPDATE_CONFIG_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case UPDATE_CONFIG_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case DELETE_CONFIG:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case DELETE_CONFIG_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case DELETE_CONFIG_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case DELETE_CONFIG_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }

	default:
		return state;
  }
}
