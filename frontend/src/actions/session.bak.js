import * as types from '../constants/ActionTypes'
import { API_ACTIVE, API_ENDPOINT } from '../constants/Globals'

import Promise from 'bluebird'
import axios from 'axios'
import store from 'store'
import jwtDecode from 'jwt-decode'

function _fetchToken(dispatch, data) {
  return new Promise((resolve, reject) => {
    dispatch(fetchToken(data)).then(resolve).catch(reject)
  })
}

function _renewToken(dispatch, token) {
  return new Promise((resolve, reject) => {
    dispatch(renewToken(token)).then(resolve).catch(reject)
  })
}

function _fetchUser(dispatch, token, params) {
  return new Promise((resolve, reject) => {
    dispatch(fetchUser(token, params)).then(resolve).catch(reject)
  })
}

export function fetchToken(data) {
  const request = axios({
    method: 'post',
    data: data,
    url: `${API_ENDPOINT}/token`,
    headers: []
  })

  return {
    type: types.SESSION_FETCH_TOKEN,
    payload: request
  }
}

export function renewToken(token) {
  const request = axios({
    method: 'post',
    data: { token: token },
    url: `${API_ENDPOINT}/token/renew`,
    headers: []
  })

  return {
    type: types.SESSION_RENEW_TOKEN,
    payload: request
  }
}

export function fetchUser(token, params) {
  const request = axios({
    method: 'get',
    url: `${API_ENDPOINT}/users/${params.id}`,
    headers: {
      'X-Access-Token': token
    },
  })

  return {
    type: types.SESSION_FETCH_USER,
    payload: request
  }
}

export function login(data) {
  return async dispatch => {
    const fetchTokenResponse = await _fetchToken(dispatch, data).catch(console.log)
    const token = fetchTokenResponse.value.data.token

    if (token) {
      const renewTokenResponse = await _renewToken(dispatch, token).catch(console.log)
      const newToken = renewTokenResponse.value.data.token

      if (newToken) {
        const tokenData = jwtDecode(newToken)
        const userId = tokenData.id
        const fetchUserResponse = await _fetchUser(dispatch, newToken, { id: userId }).catch(console.log)
        const userData = fetchUserResponse.value.data

        store.set('token', newToken)
        store.set('user', userData)
      }
    }

    return fetchTokenResponse
  }
}

export function verifyToken(token) {
  return async dispatch => {
    const renewTokenResponse = await _renewToken(dispatch, token).catch(console.log)
    const newToken = renewTokenResponse.value.data.token

    if (newToken) {
      const tokenData = jwtDecode(newToken)
      const userId = tokenData.id
      const fetchUserResponse = await _fetchUser(dispatch, newToken, { id: userId }).catch(console.log)
      const userData = fetchUserResponse.value.data

      store.set('token', newToken)
      store.set('user', userData)
    }

    return renewTokenResponse
  }
}

export function logout() {
  return async dispatch => {
    store.remove('token')
    store.remove('user')

    dispatch({
      type: types.SESSION_LOGOUT,
      payload: null
    })
  }
}

