import axios from 'axios'
import { API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function searchUsers(params) {
	let search = `?q=${params.query}`

	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/users/search${search}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.SEARCH_USERS,
		payload: request
	}
}

export function fetchUsers() {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/users`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_USERS,
		payload: request
	}
}

export function fetchUser(params) {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/users/${params.id}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_USER,
		payload: request
	}
}

export function createUser(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${API_ENDPOINT}/users`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.CREATE_USER,
		payload: request
	}
}

export function updateUser(user, props) {
	const request = axios({
		method: 'put',
		data: props,
		url: `${API_ENDPOINT}/users/${user}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.UPDATE_USER,
		payload: request
	}
}

export function deleteUser(user) {
	const request = axios({
		method: 'delete',
		url: `${API_ENDPOINT}/users/${user}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.DELETE_USER,
		payload: request
	}
}