import React, { Component } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import update from 'immutability-helper'
import { CSSTransitionGroup } from 'react-transition-group'

import Navbar from '@/components/Navbar'

import Logout from '@/pages/Logout'
import Control from '@/pages/Control'
import Collections from '@/pages/Collections'
import NewCollection from '@/pages/NewCollection'
import Collection from '@/pages/Collection'
import EditCollection from '@/pages/EditCollection'

@connect((state) => {
  return {}
}, (dispatch, ownProps) => {
  return {}
})
export default class AuthorizedLayout extends Component {
  constructor(props) {
    super(props)

    this.state = {
      location: null,
    }
  }

  componentDidMount() {
    const { location } = this.props

    this.setLocation(location)
  }

  componentWillReceiveProps(nextProps) {
    const { location } = nextProps

    this.setLocation(location)
  } 

  setLocation(location) {
    this.setState((prevState, props) => {
      return update(prevState, {
        $merge: { location: location }
      })
    })
  }

  render() {
    return (
      <div className="app">
        <header className="header">
          <Navbar location={this.state.location} user={this.state.user}/>
        </header>

        <Switch>
          <Route path="/control/collections/create" component={NewCollection}/>
          <Route path="/control/collections" component={Collections}/>
          <Route path="/control" component={Control}/>
          <Route path="/logout" component={Logout}/>
        </Switch>
      </div>
    )
  }
}
