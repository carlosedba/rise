import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import update from 'immutability-helper'

export default class Modal extends Component {
  constructor(props) {
    super(props)

    this.open = this.open.bind(this)
    this.close = this.close.bind(this)
  }

  componentDidMount() {}

  open() {}

  close() {}

  render() {
    return (
      <div className="modal-wrapper">
        <div onClick={this.close} className="modal-background"></div>
        <div className="modal">
          {React.cloneElement(this.props.children, { returnTo: this.props.returnTo })}
          <Link to={this.props.returnTo} className="modal-close">&#10006;</Link>
        </div>
      </div>
    )
  }
}




