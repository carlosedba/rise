import React, { Component } from 'react'
import { connect } from 'react-redux'

import '../assets/css/widgetFields.css'

@connect((store) => {
  return {}
})
export default class WidgetFields extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="widget" styleName="widget-fields">
		<div className="widget-header">
			<div className="widget-header-row">
				<div className="widget-header-left">
					<p className="widget-title">Campos</p>
					<a className="btn hidden" href="">Novo campo</a>
				</div>
				<div className="widget-header-right"></div>
			</div>
			<div className="widget-header-row no-pad">
				<div className="widget-list-header">
					<span className="attribute" data-attribute="nome">Nome</span>
					<span className="attribute" data-attribute="descricao">Descrição</span>
					<span className="attribute" data-attribute="atributo">Atributo</span>
					<span className="attribute" data-attribute="tipo">Tipo</span>
					<span className="attribute" data-attribute="actions"></span>
				</div>
			</div>
		</div>
		<div className="widget-content nopad">
			<div className="widget-list">
				<div className="widget-list-items">
					<div className="new-item">
						<div className="icon">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42 42">
								<path fill="#000" d="M42 19H23V0h-4v19H0v4h19v19h4V23h19"/>
							</svg>
						</div>
						<span className="text">Novo campo</span>
					</div>
					<div className="item">
						<span className="attribute" data-attribute="nome">Título</span>
						<span className="attribute" data-attribute="descricao"></span>
						<span className="attribute" data-attribute="atributo">title</span>
						<span className="attribute" data-attribute="tipo">Texto</span>
						<div className="actions">
							<a className="action" href="javascript:void(0)">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 432.5 432.5">
									<path d="M0 313.8v118.7h118.8L356.3 195 237.5 76.2 0 313.8zM103.6 396H73v-36.5H36.6V329l26-26 67 67-26 26zm143-271.2c4.3 0 6.4 2 6.4 6.2 0 2-.7 3.6-2 5L96.2 290.6c-1.3 1.3-3 2-4.8 2-4.2 0-6.3-2.2-6.3-6.3 0-2 .7-3.6 2-5L242 127c1.4-1.4 3-2 5-2zM422 77.7l-67-67C347.6 3.8 339 0 329 0c-10.4 0-19 3.6-25.8 10.8L255.8 58l118.8 118.7 47.4-47.4c7-7 10.5-15.6 10.5-25.7 0-10-3.5-18.5-10.5-26z"/>
								</svg>
							</a>
							<a className="action" href="javascript:void(0)">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
									<path fill="#000" d="M425.3 51.4h-91.5V16.7C333.8 7.5 326.4 0 317 0H195C185.5 0 178 7.5 178 16.7v34.7H86.7C77.5 51.4 70 58.8 70 68v51.4c0 9.2 7.5 16.7 16.7 16.7h338.6c9.2 0 16.7-7.4 16.7-16.6V68c0-9.2-7.5-16.6-16.7-16.6zm-125 0h-88.8v-18h89v18zM93.2 169.5L107 496c.4 9 7.8 16 16.7 16h264.6c9 0 16.3-7 16.7-16l13.8-326.5H93.2zM205.5 444c0 9.3-7.4 16.8-16.7 16.8-9.2 0-16.7-7.5-16.7-16.7V237.5c0-9.2 7.6-16.7 16.8-16.7 9.3 0 16.7 7.5 16.7 16.7V444zm67.2 0c0 9.3-7.5 16.8-16.7 16.8s-16.7-7.5-16.7-16.7V237.5c0-9.2 7.5-16.7 16.7-16.7s16.7 7.5 16.7 16.7V444zm67.2 0c0 9.3-7.6 16.8-16.8 16.8s-16.7-7.5-16.7-16.7V237.5c0-9.2 7.4-16.7 16.7-16.7s16.7 7.5 16.7 16.7V444z"/>
								</svg>
							</a>
						</div>
					</div>
					<div className="item">
						<span className="attribute" data-attribute="nome">Descrição</span>
						<span className="attribute" data-attribute="descricao"></span>
						<span className="attribute" data-attribute="atributo">description</span>
						<span className="attribute" data-attribute="tipo">Texto</span>
						<div className="actions">
							<a className="action" href="javascript:void(0)">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 432.5 432.5">
									<path d="M0 313.8v118.7h118.8L356.3 195 237.5 76.2 0 313.8zM103.6 396H73v-36.5H36.6V329l26-26 67 67-26 26zm143-271.2c4.3 0 6.4 2 6.4 6.2 0 2-.7 3.6-2 5L96.2 290.6c-1.3 1.3-3 2-4.8 2-4.2 0-6.3-2.2-6.3-6.3 0-2 .7-3.6 2-5L242 127c1.4-1.4 3-2 5-2zM422 77.7l-67-67C347.6 3.8 339 0 329 0c-10.4 0-19 3.6-25.8 10.8L255.8 58l118.8 118.7 47.4-47.4c7-7 10.5-15.6 10.5-25.7 0-10-3.5-18.5-10.5-26z"/>
								</svg>
							</a>
							<a className="action" href="javascript:void(0)">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
									<path fill="#000" d="M425.3 51.4h-91.5V16.7C333.8 7.5 326.4 0 317 0H195C185.5 0 178 7.5 178 16.7v34.7H86.7C77.5 51.4 70 58.8 70 68v51.4c0 9.2 7.5 16.7 16.7 16.7h338.6c9.2 0 16.7-7.4 16.7-16.6V68c0-9.2-7.5-16.6-16.7-16.6zm-125 0h-88.8v-18h89v18zM93.2 169.5L107 496c.4 9 7.8 16 16.7 16h264.6c9 0 16.3-7 16.7-16l13.8-326.5H93.2zM205.5 444c0 9.3-7.4 16.8-16.7 16.8-9.2 0-16.7-7.5-16.7-16.7V237.5c0-9.2 7.6-16.7 16.8-16.7 9.3 0 16.7 7.5 16.7 16.7V444zm67.2 0c0 9.3-7.5 16.8-16.7 16.8s-16.7-7.5-16.7-16.7V237.5c0-9.2 7.5-16.7 16.7-16.7s16.7 7.5 16.7 16.7V444zm67.2 0c0 9.3-7.6 16.8-16.8 16.8s-16.7-7.5-16.7-16.7V237.5c0-9.2 7.4-16.7 16.7-16.7s16.7 7.5 16.7 16.7V444z"/>
								</svg>
							</a>
						</div>
					</div>
					<div className="item">
						<span className="attribute" data-attribute="nome">Categoria</span>
						<span className="attribute" data-attribute="descricao"></span>
						<span className="attribute" data-attribute="atributo">category</span>
						<span className="attribute" data-attribute="tipo">Texto</span>
						<div className="actions">
							<a className="action" href="javascript:void(0)">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 432.5 432.5">
									<path d="M0 313.8v118.7h118.8L356.3 195 237.5 76.2 0 313.8zM103.6 396H73v-36.5H36.6V329l26-26 67 67-26 26zm143-271.2c4.3 0 6.4 2 6.4 6.2 0 2-.7 3.6-2 5L96.2 290.6c-1.3 1.3-3 2-4.8 2-4.2 0-6.3-2.2-6.3-6.3 0-2 .7-3.6 2-5L242 127c1.4-1.4 3-2 5-2zM422 77.7l-67-67C347.6 3.8 339 0 329 0c-10.4 0-19 3.6-25.8 10.8L255.8 58l118.8 118.7 47.4-47.4c7-7 10.5-15.6 10.5-25.7 0-10-3.5-18.5-10.5-26z"/>
								</svg>
							</a>
							<a className="action" href="javascript:void(0)">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
									<path fill="#000" d="M425.3 51.4h-91.5V16.7C333.8 7.5 326.4 0 317 0H195C185.5 0 178 7.5 178 16.7v34.7H86.7C77.5 51.4 70 58.8 70 68v51.4c0 9.2 7.5 16.7 16.7 16.7h338.6c9.2 0 16.7-7.4 16.7-16.6V68c0-9.2-7.5-16.6-16.7-16.6zm-125 0h-88.8v-18h89v18zM93.2 169.5L107 496c.4 9 7.8 16 16.7 16h264.6c9 0 16.3-7 16.7-16l13.8-326.5H93.2zM205.5 444c0 9.3-7.4 16.8-16.7 16.8-9.2 0-16.7-7.5-16.7-16.7V237.5c0-9.2 7.6-16.7 16.8-16.7 9.3 0 16.7 7.5 16.7 16.7V444zm67.2 0c0 9.3-7.5 16.8-16.7 16.8s-16.7-7.5-16.7-16.7V237.5c0-9.2 7.5-16.7 16.7-16.7s16.7 7.5 16.7 16.7V444zm67.2 0c0 9.3-7.6 16.8-16.8 16.8s-16.7-7.5-16.7-16.7V237.5c0-9.2 7.4-16.7 16.7-16.7s16.7 7.5 16.7 16.7V444z"/>
								</svg>
							</a>
						</div>
					</div>
					<div className="item">
						<span className="attribute" data-attribute="nome">Conteúdo</span>
						<span className="attribute" data-attribute="descricao"></span>
						<span className="attribute" data-attribute="atributo">content</span>
						<span className="attribute" data-attribute="tipo">Texto</span>
						<div className="actions">
							<a className="action" href="javascript:void(0)">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 432.5 432.5">
									<path d="M0 313.8v118.7h118.8L356.3 195 237.5 76.2 0 313.8zM103.6 396H73v-36.5H36.6V329l26-26 67 67-26 26zm143-271.2c4.3 0 6.4 2 6.4 6.2 0 2-.7 3.6-2 5L96.2 290.6c-1.3 1.3-3 2-4.8 2-4.2 0-6.3-2.2-6.3-6.3 0-2 .7-3.6 2-5L242 127c1.4-1.4 3-2 5-2zM422 77.7l-67-67C347.6 3.8 339 0 329 0c-10.4 0-19 3.6-25.8 10.8L255.8 58l118.8 118.7 47.4-47.4c7-7 10.5-15.6 10.5-25.7 0-10-3.5-18.5-10.5-26z"/>
								</svg>
							</a>
							<a className="action" href="javascript:void(0)">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
									<path fill="#000" d="M425.3 51.4h-91.5V16.7C333.8 7.5 326.4 0 317 0H195C185.5 0 178 7.5 178 16.7v34.7H86.7C77.5 51.4 70 58.8 70 68v51.4c0 9.2 7.5 16.7 16.7 16.7h338.6c9.2 0 16.7-7.4 16.7-16.6V68c0-9.2-7.5-16.6-16.7-16.6zm-125 0h-88.8v-18h89v18zM93.2 169.5L107 496c.4 9 7.8 16 16.7 16h264.6c9 0 16.3-7 16.7-16l13.8-326.5H93.2zM205.5 444c0 9.3-7.4 16.8-16.7 16.8-9.2 0-16.7-7.5-16.7-16.7V237.5c0-9.2 7.6-16.7 16.8-16.7 9.3 0 16.7 7.5 16.7 16.7V444zm67.2 0c0 9.3-7.5 16.8-16.7 16.8s-16.7-7.5-16.7-16.7V237.5c0-9.2 7.5-16.7 16.7-16.7s16.7 7.5 16.7 16.7V444zm67.2 0c0 9.3-7.6 16.8-16.8 16.8s-16.7-7.5-16.7-16.7V237.5c0-9.2 7.4-16.7 16.7-16.7s16.7 7.5 16.7 16.7V444z"/>
								</svg>
							</a>
						</div>
					</div>
					<div className="item">
						<span className="attribute" data-attribute="nome">Imagem</span>
						<span className="attribute" data-attribute="descricao"></span>
						<span className="attribute" data-attribute="atributo">picture</span>
						<span className="attribute" data-attribute="tipo">Imagem</span>
						<div className="actions">
							<a className="action" href="javascript:void(0)" data-id="1" data-modal="ModalEditAttribute">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 432.5 432.5">
									<path d="M0 313.8v118.7h118.8L356.3 195 237.5 76.2 0 313.8zM103.6 396H73v-36.5H36.6V329l26-26 67 67-26 26zm143-271.2c4.3 0 6.4 2 6.4 6.2 0 2-.7 3.6-2 5L96.2 290.6c-1.3 1.3-3 2-4.8 2-4.2 0-6.3-2.2-6.3-6.3 0-2 .7-3.6 2-5L242 127c1.4-1.4 3-2 5-2zM422 77.7l-67-67C347.6 3.8 339 0 329 0c-10.4 0-19 3.6-25.8 10.8L255.8 58l118.8 118.7 47.4-47.4c7-7 10.5-15.6 10.5-25.7 0-10-3.5-18.5-10.5-26z"/>
								</svg>
							</a>
							<a className="action" href="javascript:void(0)" data-id="1" data-modal="ModalRemoveAttribute">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
									<path fill="#000" d="M425.3 51.4h-91.5V16.7C333.8 7.5 326.4 0 317 0H195C185.5 0 178 7.5 178 16.7v34.7H86.7C77.5 51.4 70 58.8 70 68v51.4c0 9.2 7.5 16.7 16.7 16.7h338.6c9.2 0 16.7-7.4 16.7-16.6V68c0-9.2-7.5-16.6-16.7-16.6zm-125 0h-88.8v-18h89v18zM93.2 169.5L107 496c.4 9 7.8 16 16.7 16h264.6c9 0 16.3-7 16.7-16l13.8-326.5H93.2zM205.5 444c0 9.3-7.4 16.8-16.7 16.8-9.2 0-16.7-7.5-16.7-16.7V237.5c0-9.2 7.6-16.7 16.8-16.7 9.3 0 16.7 7.5 16.7 16.7V444zm67.2 0c0 9.3-7.5 16.8-16.7 16.8s-16.7-7.5-16.7-16.7V237.5c0-9.2 7.5-16.7 16.7-16.7s16.7 7.5 16.7 16.7V444zm67.2 0c0 9.3-7.6 16.8-16.8 16.8s-16.7-7.5-16.7-16.7V237.5c0-9.2 7.4-16.7 16.7-16.7s16.7 7.5 16.7 16.7V444z"/>
								</svg>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
      </div>
    )
  }
}

