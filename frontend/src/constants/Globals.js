export const PRODUCTION = false
export const SERVER_PORT = location.port
export const SERVER_ADDRESS = (PRODUCTION) ? '' : `http://server.com/rise/server/public_html/dev.php`
export const API_ENDPOINT = (PRODUCTION) ? '' : `${SERVER_ADDRESS}/api/v1`

