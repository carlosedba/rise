import { PRODUCTION } from '../constants/Globals'

import { combineReducers, createStore, applyMiddleware, compose } from 'redux'
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'
import promise from 'redux-promise-middleware'

import reducer from '../reducers'

export default function setStore(initialState) {
	const reduxPromiseMiddleware = promise({ promiseTypeSuffixes: ['LOADING', 'SUCCESS', 'ERROR'] })
	const middleware = (PRODUCTION)
						? applyMiddleware(thunk, reduxPromiseMiddleware)
						: applyMiddleware(createLogger(), thunk, reduxPromiseMiddleware)
	const composeEnhancers = (!PRODUCTION) ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose : compose
	const store = createStore(reducer, initialState, composeEnhancers(middleware))

	if (!PRODUCTION && module.hot) {
		module.hot.accept('../reducers', () => {
			store.replaceReducer(require('../reducers'))
		})
	}

	return store
}