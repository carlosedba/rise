const path = require('path')
const webpack = require('webpack')


module.exports = function (env) {
	return {
		entry: {
			'app': './src/index.js'
		},

		output: {
			filename: '[name].bundle.js',
			path: path.resolve(__dirname, 'dist'),
			publicPath: './',
			sourceMapFilename: '[name].map'
		},
		
		resolve: {
			extensions: ['.js', '.json'],
			alias: {
				'@': path.resolve(__dirname, 'src')
			}
		},

		target: 'web',

		node: {
			__dirname: true
		},

		module: {
			rules: [
				{
					test: /\.(js|jsx)?$/,
					exclude: [
						path.resolve(__dirname, './node_modules')
					],
					loader: 'babel-loader'
				},
				{
					test: /\.(js)?$/,
					exclude: [
						path.resolve(__dirname, './node_modules')
					],
					include: [
						path.resolve(__dirname, './src/libs')
					],
					loader: 'script-loader'
				},
				{
					test:   /(\.global\.css$|react-select.css)/,
					use: [
						'style-loader',
						'css-loader',
					]
				},
				{
					test:   /^((?!\.global|react-select).)*\.css$/,
					use: [
						'style-loader',
						{ loader: 'css-loader',
							options: {
								importLoader: 1,
								modules: true,
								localIdentName: '[name]_[local]__[hash:base64:5]'
							}
						},
					]
				},
				/*
				{
					test:   /\.scss?$/,
					use: [
						'style-loader',
						'css-loader',
						'sass-loader'
					]
				},
				*/
				{
					test: /\.(png|jpg|jpeg)?$/,
					use: { loader: 'url-loader', options: { limit: 100000 } }
				},
				{ 
					test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
					use: { loader: 'url-loader', options: { limit: 100000, mimetype: 'application/font-woff' } }
				},
				{ 
					test: /\.(swoff|woff|woff2|eot|ttf|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
					loader: 'file-loader'
				},
				{ 
					test: /\.svg$/,
					loader: 'svg-inline-loader'
				}
			]
		},

		plugins: [
			new webpack.DefinePlugin({
				VERSION: JSON.stringify('1.0.0')
			})
		]
	}
}

