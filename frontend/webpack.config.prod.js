const path = require('path')
const webpack = require('webpack')
const CompressionPlugin = require('compression-webpack-plugin')
const merge = require('webpack-merge')

const base = require('./webpack.config.base')

module.exports = function (env) {
	return merge(base(), {
		mode: 'production',

		devtool: false,

		optimization: {
			minimize: true,
			splitChunks: {
				chunks: 'all',
				automaticNameDelimiter: '.',
			},
		},

		plugins: [
			new webpack.DefinePlugin({
				PRODUCTION: true,
			}),

			new webpack.EnvironmentPlugin({
				NODE_ENV: 'production'
			}),

			new CompressionPlugin({
				asset: '[path].gz[query]',
				algorithm: 'gzip',
				test: /\.(js|html)?$/,
				threshold: 0,
				minRatio: 0.8
			})
		]
	})
}

