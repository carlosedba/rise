const path = require('path')
const rm = require('rimraf')

rm(path.resolve(__dirname, 'dist'), err => {
	if (err) throw err
})

module.exports = function (env) {
	return require(`./webpack.config.${env}`)({ env: env })
}

