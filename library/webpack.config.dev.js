const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const CompressionPlugin = require('compression-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin')
const base = require('./webpack.config.base')


module.exports = function (env) {
	return merge(base(), {
		devtool: 'cheap-module-eval-source-map',
		
		devServer: {
			hot: true,
			contentBase: path.join(__dirname, 'dist'),
			publicPath: '/',
			compress: true,
			port: 3000,
			historyApiFallback: true
		},

		plugins: [
			new webpack.NamedModulesPlugin(),
			new webpack.HotModuleReplacementPlugin(),
			
			new webpack.optimize.UglifyJsPlugin({
				beautify: true,

				mangle: {
					screw_ie8: true,
					keep_fnames: true
				},

				compress: {
					screw_ie8: true
				},

				comments: true,

				sourceMap: this.devtool && (this.devtool.indexOf('sourcemap') >= 0 || this.devtool.indexOf('source-map') >= 0)
			}),

			new ExtractTextPlugin({
				filename: path.resolve(__dirname, 'css/[name].[contenthash].css')
			}),

			new OptimizeCSSPlugin({
				cssProcessorOptions: {
					safe: true
				}
			}),
		]
	})
}

