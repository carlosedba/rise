const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const CompressionPlugin = require('compression-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin')
const base = require('./webpack.config.base')

module.exports = function ({ env }) {
	return merge(base(), {
		devtool: false,

		output: {
			filename: '[name].[chunkhash].js'
		},

		plugins: [
			new webpack.optimize.UglifyJsPlugin({
				beautify: false,

				compress: {
					screw_ie8: true
				},

				comments: false,

				mangle: {
					screw_ie8: true,
					keep_fnames: true
				},

				sourceMap: true,
			}),

			new ExtractTextPlugin({
				filename: path.resolve(__dirname, 'css/[name].[contenthash].css')
			}),

			new OptimizeCSSPlugin({
				cssProcessorOptions: {
					safe: true
				}
			}),

			new webpack.optimize.CommonsChunkPlugin({
				name: 'vendor',
				minChunks: function (module, count) {
					// any required modules inside node_modules are extracted to vendor
					return (
						module.resource &&
						/\.js$/.test(module.resource) &&
						module.resource.indexOf(path.resolve(__dirname, 'node_modules')) === 0 ||
						module.resource.indexOf(path.resolve(__dirname, 'src/vendor')) === 0
					)
				}
			}),

			new webpack.optimize.CommonsChunkPlugin({
				name: 'manifest',
				chunks: ['vendor']
			}),

			new CompressionPlugin({
				asset: '[path].gz[query]',
				algorithm: 'gzip',
				test: /\.(js|html)?$/,
				threshold: 0,
				minRatio: 0.8
			})
		]
	})
}

