export default class Loading {
	static activate() {
		const loading = document.querySelector('.loading')
		loading.classList.add('visible')
	}

	static deactivate() {
		const loading = document.querySelector('.loading')
		loading.classList.remove('visible')
	}
}