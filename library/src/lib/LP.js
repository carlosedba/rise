import Event from '@/utils/Event'
import Generic from '@/utils/Generic'

export default class LP {
	constructor() {
		console.log('log > SistemaFiep > LP - initialized!')

		this.windowWidth = window.innerWidth
		
		this.player = null

		this.Event = new Event()
		this.Event.addHandler('handlePlayClick', this.handlePlayClick, this)
		this.Event.addHandler('initYoutubePlayer', this.initYoutubePlayer, this)

		document.addEventListener('DOMContentLoaded', this.render.bind(this))
		window.addEventListener('resize', this.setPlayerSize.bind(this))
	}

	initYoutubePlayer() {
		console.log('log > SistemaFiep > LP - initializing YouTube Player')

		this.player = new YT.Player(this.player, {
			width: '304',
			height: '171',
			videoId: this.player.dataset.videoId,
			playerVars: {
				rel: 0,
				showinfo: 0,
			},
			events: {
				onReady: function (event) { this.onPlayerReady(event) }.bind(LP.prototype),
				onStateChange: function (event) { this.onPlayerStateChange(event) }.bind(LP.prototype),
			}	
		})

		this.setPlayerSize()
	}

	onPlayerReady(event) {
		console.log('log > SistemaFiep > LP - YouTube Player Event: ready')
	}

	onPlayerStateChange(event) {
		console.log('log > SistemaFiep > LP - YouTube Player Event: stateChange')
	}

	handlePlayClick(event) {
		let cover = document.querySelector('.page-video-cover')

		cover.classList.add('hide')

		setTimeout(function () { cover.style.zIndex = '-1' }, 250)

		this.player.playVideo()
	}

	setPlayerSize(event) {	
		if (this.player !== null) {
			if (Modernizr.mq('(min-width: 370px)')) {
				this.player.setSize('368', '207')
			}

			if (Modernizr.mq('(min-width: 420px)')) {
				this.player.setSize('368', '207')
			}

			if (Modernizr.mq('(min-width: 520px)')) {
				this.player.setSize('512', '288')
			}

			if (Modernizr.mq('(min-width: 767px)')) {
				this.player.setSize('512', '288')
			}
		}
	}

	render(event) {
		console.log('log > SistemaFiep > LP - render called!')

		if (Generic.shouldRender(this.windowWidth, event)) {
			console.log('log > SistemaFiep > LP - render approved!')

			this.player = document.querySelector('.page-video-player')

			if (this.player !== null && this.player !== undefined && this.player.dataset.videoId !== null && this.player.dataset.videoId !== undefined) {
				let play = document.querySelector('.page-video-cover .play')

				this.Event.addTo(play, 'click', 'handlePlayClick')
				this.Event.addTo(document, 'YouTubeIframeAPIReady', 'initYoutubePlayer')
			}
		}
	}
}