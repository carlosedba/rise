import Event from '@/utils/Event'
import Generic from '@/utils/Generic'

export default class Menu {
	constructor() {
		console.log('log > SistemaFiep > Menu - initialized!')

		this.windowWidth = window.innerWidth

		this.buscaOpen = false

		this.Event = new Event()
		this.Event.addHandler('handleClick', this.handleClick)
		this.Event.addHandler('handleSubmeuClick', this.handleSubmeuClick)
		this.Event.addHandler('handleSearchClick', this.handleSearchClick)
		this.Event.addHandler('handleSearchSubmit', this.handleSearchSubmit)

		document.addEventListener('DOMContentLoaded', this.render.bind(this))
		window.addEventListener('resize', this.render.bind(this))
	}

	addSubmenuDetails() {
		let menuItems = [].slice.call(document.querySelectorAll('.navbar-menu li'))

		menuItems.map(function (el, i) {
			if (el.querySelector('.submenu') !== null) {
				if (!el.classList.contains('with-submenu')) {
					el.classList.add('with-submenu')
				}
			}
		})	
	}

	clearMenu() {
		let menu = document.querySelector('.navbar-menu')
		let withSubmenu = menu.querySelectorAll('.with-submenu')
		let submenu = menu.querySelectorAll('.submenu')
		let hamburger = document.querySelector('.hamburger')

		;[].forEach.call(withSubmenu, function (el, i) {
			el.classList.remove('open')
			submenu[i].classList.remove('open')
		})
		
		menu.classList.remove('active')
		hamburger.classList.remove('is-active')
	}

	handleClick(event) {
		let menu = document.querySelector('.navbar-menu')
		let hamburger = document.querySelector('.hamburger')

		hamburger.classList.toggle('is-active')
		menu.classList.toggle('active')
	}

	handleSubmeuClick(event) {
		let target = event.target.parentNode
		let submenu = target.querySelector('.submenu')

		if (submenu !== null && submenu !== undefined) {
			target.classList.toggle('open')
			submenu.classList.toggle('open')
		}
	}

	handleSearchClick(event) {
		const navsearch = document.querySelector('.navsearch')
		const navsearchBefore = CSSRulePlugin.getRule('.navsearch::before')

		if (this.buscaOpen) {
			TweenMax.to(navsearch, 0.320, {
				y: 0,
				ease: Power4.easeOut,
			})

			TweenMax.to(navsearchBefore, 0.120, {
				cssRule: {
					height: 0,
				},
			})
		} else {
			TweenMax.fromTo(navsearch, 0.320, {
				y: 0,
				ease: Power4.easeOut,
			}, {
				y: 80,
				ease: Power4.easeOut,
			})

			TweenMax.to(navsearchBefore, 0.320, {
				cssRule: {
					height: '1px',
				},
			})
		}

		this.buscaOpen = !this.buscaOpen
	}

	handleSearchSubmit(event) {
		event.preventDefault()
		const navsearchInput = document.querySelector('.navsearch input')

		location.href = location.origin + '/busca?query=' + navsearchInput.value
	}

	render(event) {
		console.log('log > SistemaFiep > Menu - render called!')
		
		if (Generic.shouldRender(this.windowWidth, event)) {
			console.log('log > SistemaFiep > Menu - render approved!')

			let hamburger = document.querySelector('.hamburger')
			let menuItems = document.querySelectorAll('.navbar-menu li')
			let search = document.querySelector('.navbar-menu .js-search')
			let navsearch = document.querySelector('.navsearch form')
			let navsearchButton = document.querySelector('.navsearch button')

			if (Modernizr.mq('(max-width: 979px)')) {
				console.log('log > SistemaFiep > Menu - events added!')
				this.Event.addTo(hamburger, 'click', 'handleClick')
				this.Event.addTo(menuItems, 'click', 'handleSubmeuClick')
				this.clearMenu()
			} else {
				console.log('log > SistemaFiep > Menu - events removed!')
				this.Event.removeFrom(hamburger, 'click', 'handleClick')
				this.Event.removeFrom(menuItems, 'click', 'handleSubmeuClick')
			}

			if (search) this.Event.addTo(search, 'click', 'handleClick')
			if (search) this.Event.addTo(search, 'click', 'handleSearchClick')
			if (navsearch) this.Event.addTo(navsearch, 'submit', 'handleSearchSubmit')
			if (navsearchButton) this.Event.addTo(navsearchButton, 'click', 'handleSearchSubmit')

			this.addSubmenuDetails()
		}
	}
}

























