var merge = require('webpack-merge');
var prodEnv = require('./prod.env');

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  SERVER_LOCATION: '"http://localhost:8181/"',
  API_LOCATION: '"http://localhost:8181/api"',
});
